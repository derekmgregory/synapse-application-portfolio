READ ME

This Portfolio collection contains a few example projects from a recent Operating Systems class that focused mainly on Linux (or at least POSIX) kernal interactions. There is a Makefile included in each sub-directory.

The following is a brief summary of each program included:

	-Dining Philosophers: 
		-This is an exercise with the classic race condition / deadlock paradox. It uses a resource hierarchy as a solution and models each philosopher as a seperate thread using the pthread library.

	-Linux Kernal Monitor:
		-This is a small program to report certain behaviour of the linux kernal, such as processor type, kernal version, time since last boot etc.
		NOTE: this must be run on a linux kernal in order for correct operation.

		BASE COMMAND: ksamp

		OPTIONS:
			-NONE:
				-eg. "./ksamp"
				-Provides: processor type, kernal version, time since last boot

			-SPECIFIC: '-s'
				-eg. "./ksamp -s"
				-Provides: All of the above + 
							-Time in user/system/idle mode
							-Disk read/writes
							-Context switches
							-Time of Last Boot
							-Number of processes created

			-LOAD: '-l $arg1 $arg2'
				-eg. "./ksamp -l 5 15"
				-Provides: All of the above + 
							-Total memory in the system
							-Free memory in the system
							-Load average samples taken every $arg1 seconds for a duration of $arg2 seconds

	-Virtual Memory Manager:
		-This is a virtual memory manager that mimics how memory is actually handled by the linux kernal. This includes address translation as well as having a Table Lookaside Buffer. There are simulated memory accesses given in addresses.txt, with each memory access being given its own thread to run in. As well, there is a file 'BACKING_STORE.bin' that acts as our simulated physical memory.

		USAGE:  './VMem addresses.txt'

	-Process Dispatcher:
		-The goal of this was to design a Hypothetical Operating System Testbed (HOST) Dispatcher Shell, which is a process dispatching system based on a 4 level process priority queue. They were designated b/w a "real time" process with highest priority, and then a 3 level queue with priority decremented after every completed time quantum. Each process is forked from the parent and contains its own memory space. An example process dispatch list is provided, simply called 'dispatchlist1'.

		USAGE: './hostd dispatchlist1'


		
