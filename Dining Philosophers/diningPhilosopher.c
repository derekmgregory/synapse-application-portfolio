#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


#define VERBOSE 1
#define NUMPHILOSOPHERS 5
#define DEFAULTSLEEP 1000000
#define RANDOMSLEEP 2000000

/* Represents fork */
struct fork {
    pthread_mutex_t lock;
    pthread_cond_t forkFree;
    long isFree;
} forks[NUMPHILOSOPHERS];
    
void threadSleep() {
    usleep(DEFAULTSLEEP);
    usleep(random() % RANDOMSLEEP );
}

/* Returns the minimum of a and b, or a if equal */
int getMinimum(int a, int b){
    
    if(a <= b)
        return a;
    else 
        return b;
}

/* Returns the maximum of a and b, or a if equal */
int getMaximum(int a, int b){
    
    if(a < b)
        return b;
    else
        return a;
}

/* Acquires fork i using isFree variable protected by mutex */
int pick_fork_up(int i) {
    pthread_mutex_lock(&forks[i].lock);
    while (!forks[i].isFree) {
       pthread_cond_wait(&forks[i].forkFree, &forks[i].lock);
    }
    forks[i].isFree = 0;
    pthread_mutex_unlock(&forks[i].lock);
    return 1;
}

/* Releases fork i using isFree variable protected by mutex */
void put_fork_down(int i) {

    pthread_mutex_lock(&forks[i].lock);
    forks[i].isFree = 1;
    pthread_cond_signal(&forks[i].forkFree);
    pthread_mutex_unlock(&forks[i].lock);
}


int leftfork(int i) {
    return i;
}

int rightfork(int i) {
    return (i + 1) % NUMPHILOSOPHERS;
}

/* Acquires both forks using resource ordering, prevents deadlock */
void pickup_forks(int id){
    int forkToGet;
    forkToGet = getMinimum(leftfork(id), rightfork(id) );
    pick_fork_up(forkToGet);
    forkToGet = getMaximum(leftfork(id), rightfork(id) );
    pick_fork_up(forkToGet);
    if( VERBOSE )
        printf("Philosopher %d has acquired fork %d and fork %d and is eating\n", id, leftfork(id), rightfork(id) ); 
}   

/* Releases both forks when philosopher is done eating */
void return_forks(int id){
   
    put_fork_down(leftfork(id));
    put_fork_down(rightfork(id));
    if( VERBOSE )
        printf("Philosopher %d has released fork %d and fork %d and is thinking\n", id, leftfork(id), rightfork(id) );    
}   

/* Main thread function, alternates thread/philosopher between eating and thinking */
void *dining(void *arg) {
    long id = (long) arg;
    while (1) {
    	threadSleep();     //Thinking
        pickup_forks(id);
        threadSleep();      //Eating
        return_forks(id);
    }	
    return 0;
}

int main(int argc, char **argv) {
	pthread_t philosophers[NUMPHILOSOPHERS];
	long i;

	/* Initializes mutex's and condition variables for each fork and checks for errors */
	for (i = 0; i < NUMPHILOSOPHERS; i++) {
		if( pthread_mutex_init(&forks[i].lock, NULL) != 0) 
		    printf("ERROR: initializing mutex for fork %ld", i);
		if( pthread_cond_init(&forks[i].forkFree, NULL) != 0)
		    printf("ERROR: initializing condition variable for fork %ld", i);
		forks[i].isFree = 1;
	}
    
	/* Creates a thread for each of the philosophers and checks for errors */
	for (i = 0; i < NUMPHILOSOPHERS; i++) {
		if( pthread_create(&philosophers[i], NULL, dining, (void *)i) != 0)
			printf("ERROR: creating thread %ld", i);
	}
    
	/* Wait for all threads to complete */
	for (i = 0; i < NUMPHILOSOPHERS; ++i) {
		pthread_join(philosophers[i], 0);
	}
	return 0;
}
