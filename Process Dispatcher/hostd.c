
#include "hostd.h"

#include "mmu.h"


/* GLOBAL VARIABLES */
Process process[NUMBER_OF_PROCESSES]; /* Represents the dispatch list */

mmu mainMemory;
Dispatcher dispatcher; /* Represents a dispatcher data structure with 4 level queues */
Resources globalResources; /* Represents the global resources available */
int isDispatchListEmpty = 0; /* Flag to indicate if all processes in dispatch list have been queued */
int currentTime = 0; /* Current time counter - incremented every second */
int processCount = 0; /* Keeps track of the number of processes in dispatch list */
int numProcessesTerminated = 0; /* Keeps track of the number of processes that have finished executing */
int isrealTimeQueueEmpty = 0;

int main(int argc, char *argv[]) {

	FILE * fid;
	fpos_t position;
	int i;
	pthread_t thread1, thread2;
	int id;
	int resource_check;
	
	if(argc != 2) {
		fprintf(stderr, "ERROR: Dispatch list not specified in arguments\n");
		exit(-1);
	}
	
	/* Initialize all queues in dispatcher and initialize process arrival time 
	   and process queued status in Process struct */
	for(i = 0; i < NUMBER_OF_PROCESSES; i++) {
		dispatcher.real_time_queue.data[i] = EMPTY_ELEMENT;
		dispatcher.real_time_queue.firstEmptyIndex = 0;
		
		dispatcher.pr1_queue.data[i] = EMPTY_ELEMENT;
		dispatcher.pr1_queue.firstEmptyIndex = 0;
		
		dispatcher.pr2_queue.data[i] = EMPTY_ELEMENT;
		dispatcher.pr2_queue.firstEmptyIndex = 0;
		
		dispatcher.pr3_queue.data[i] = EMPTY_ELEMENT;
		dispatcher.pr3_queue.firstEmptyIndex = 0;
		
		process[i].arrivalTime = -1;
		process[i].queued = 0;
		process[i].alive = 0;
		process[i].memLocation = -1;
	}
	/*Initialize memory */
    memoryInitializer( &mainMemory );
	/* Initialize number of global resources available */
	globalResources.printerNum = 2;
	globalResources.scannerNum = 1;
	globalResources.modemNum = 1;
	globalResources.CDNum = 2;
	
	/* Open file specified in command line for reading */	
	fid = fopen(argv[1], "r");
	fgetpos(fid, &position);
	/* While dispatch list file has not hit end of file, call readFile function
	   to fill in process struct which represents dispatch list */
    while(fscanf(fid,"%*[^\n]\n", NULL)!= EOF) {
    	fsetpos(fid, &position);
        readFile(fid, processCount);
        processCount++;
        fgetpos(fid, &position);
    }
    fclose(fid);
    
    pthread_create(&thread1, NULL, counter_1_second, NULL);
    //pthread_create(&thread2, NULL, queueProcesses, NULL);


    /* Run until all processes have terminated */
    while(numProcessesTerminated != processCount) {

    	queueProcesses();
    	//printf("Q3: %i, %i, %i", dispatcher.pr3_queue.data[0], dispatcher.pr3_queue.data[1], dispatcher.pr3_queue.data[2]);
    	//printf("printers: %i\nscanners: %i\nmodems: %i\nCD: %i\n", globalResources.printerNum, globalResources.scannerNum, globalResources.modemNum, globalResources.CDNum);

    	/* If real time queue is not empty, then dequeue real time queue and execute process */
    	if((id = dispatcher.real_time_queue.data[0]) != EMPTY_ELEMENT) {
            if( process[id].memLocation == -1 )
            	memoryAllocator( id, &mainMemory, process);
    		printJobParams(id);
    		runProcess( id, &dispatcher.real_time_queue);	//Run the process  		
    		dequeue(&dispatcher.real_time_queue);
    	}
    	
    	/* Else if real time queue is empty and pr1 queue is not empty
    	   then execute process and dequeue pr1 queue */
    	else if((id = dispatcher.pr1_queue.data[0]) != EMPTY_ELEMENT) {
    		resource_check = resourceAvailable(dispatcher.pr1_queue.data[0]);
    		if( resource_check ){
    			if( process[id].memLocation == -1 )
                	memoryAllocator( id, &mainMemory, process);
    			printJobParams(id);
    			runProcess( id, &dispatcher.pr1_queue);
    		}

    		updateQueue( &dispatcher.pr1_queue, &dispatcher.pr2_queue, resource_check );
    	
    	}

    	/* Else if pr1 queue is empty and pr2 queue is not empty
    	   then execute process and dequeue pr2 queue */
    	else if((id = dispatcher.pr2_queue.data[0]) != EMPTY_ELEMENT) {
    		resource_check = resourceAvailable(dispatcher.pr2_queue.data[0]);
    		if(resource_check){
    			if( process[id].memLocation == -1 )
                	memoryAllocator( id, &mainMemory, process);

    			printJobParams(id );
    			runProcess( id, &dispatcher.pr2_queue);
    		}

    		updateQueue( &dispatcher.pr2_queue, &dispatcher.pr3_queue, resource_check );
    	
    	}

    	/* Else if real time queue is empty and pr1 queue is not empty
    	   then execute process and dequeue pr1 queue */
    	else if((id =dispatcher.pr3_queue.data[0]) != EMPTY_ELEMENT) {
    		resource_check = resourceAvailable(dispatcher.pr3_queue.data[0]);
    		if(resource_check){
    			if( process[id].memLocation == -1 )
              	  memoryAllocator( id, &mainMemory, process);

    			printJobParams(id);
    			runProcess( id, &dispatcher.pr3_queue);
    		}

    		updateQueue( &dispatcher.pr3_queue, &dispatcher.pr3_queue, resource_check );
    	}

    }				
	
	return 0;
}

/*
*	Runs a process
*
*/
void runProcess( int p, Queue * queue ){
	pid_t pid;
	int status;

	if( process[p].alive == 0){
		//Process needs to be created
		pid = fork();
		//Keep track of pid in case signal is suspended
		process[p].process_id = pid;

		/* Child Process */	  				
		if(pid == 0) {
			char idBuffer[10];
			char priorityBuffer[10];
			char procTimeBuffer[10];
			char statusBuffer[10];
			snprintf(idBuffer, 10, "%d", p);
			snprintf(procTimeBuffer, 10, "%d", process[p].procTime);
			snprintf(priorityBuffer, 10, "%d", process[p].priority);
			snprintf(statusBuffer, 10, "%d", process[p].alive);
			char * newargv[] = {"process", idBuffer, priorityBuffer, procTimeBuffer, statusBuffer, NULL};
			if(process[p].alive == 0)
				execv("process", newargv);
		}
		else{

		//Print pid of child
		printf("Process ID: %i\n", process[p].process_id);	
		//	Allocates resources
		globalResources.printerNum -= process[p].rsrc.printerNum;
		globalResources.scannerNum -= process[p].rsrc.scannerNum;
		globalResources.modemNum -= process[p].rsrc.modemNum;
		globalResources.CDNum -= process[p].rsrc.CDNum;
		}
    }
    else{
    	//Print pid of child
		printf("Process ID: %i\n", process[p].process_id);
    	//Process has already been created, continue with exectuion
		kill(process[p].process_id, SIGCONT);
    }		
	
	/* Parent Process */
	if(pid > 0) {
		waitpid(process[p].process_id, &status, WUNTRACED);
		if(WIFSTOPPED(status)){
		//Process was suspended		
			process[p].alive = 1;
			//Decrement processing time remaining
			process[p].procTime--;
		}
		else{
		//process was terminated
			process[p].alive = 0;
            memoryReleaser( p, &mainMemory, process);

			numProcessesTerminated++;

			//free up allocated resources
			globalResources.printerNum += process[p].rsrc.printerNum;
			globalResources.scannerNum += process[p].rsrc.scannerNum;
			globalResources.modemNum += process[p].rsrc.modemNum;
			globalResources.CDNum += process[p].rsrc.CDNum;
		}		
	}
	
	/* Fork Error */
	else {
		fprintf(stderr, "Fork failed \n");
        exit(-1);
	}

}

/* Read line in dispatch list and store in process struct */ 
void readFile(FILE* filename, int processCount) {

	fscanf(filename, "%d %d %d %d %d %d %d %d \n", 
				&process[processCount].arrivalTime, 
				&process[processCount].priority,
				&process[processCount].procTime,
				&process[processCount].Mbytes, 
				&process[processCount].rsrc.printerNum, 
				&process[processCount].rsrc.scannerNum, 
				&process[processCount].rsrc.modemNum, 
				&process[processCount].rsrc.CDNum);
				
	process[processCount].queued = 0;
	process[processCount].alive = 0;
}

/*
*	Checks if resources are already in control of process or 
*	available for the current process to run
*	Returns true if there are, false if there are not
*/
bool resourceAvailable( int p ){

	//If process is alive, already has control of resources
	if(process[p].alive == 1)
		return true;

	//Check if available  globalResourcesources
	else if( ((globalResources.printerNum - process[p].rsrc.printerNum) >= 0)
			&& ((globalResources.scannerNum - process[p].rsrc.scannerNum) >= 0)
			&& ((globalResources.modemNum - process[p].rsrc.modemNum) >= 0)
			&& ((globalResources.CDNum - process[p].rsrc.CDNum) >= 0) ){
		return true;
	}
		
	// All or some resources were not available
	else 
		return false;	

}


void printJobParams(int i) {
	//printf("\nPID %i\n", i);	//Process number, not neccessary
    printf(" Memory location: %i to %i\n", process[i].memLocation, process[i].memLocation + process[i].Mbytes);

	printf("Priority: %i\n", process[i].priority);
	printf("CPU time remaining: %i sec\n", process[i].procTime);
	printf("Memory Block Size: %i\n", process[i].Mbytes);
	printf("Resources requested: %i Printers, %i Scanners, %i Modems, %i CDs\n", 
			process[i].rsrc.printerNum, process[i].rsrc.scannerNum, process[i].rsrc.modemNum,
			process[i].rsrc.CDNum);
}

/* Increments current time every second */
void * counter_1_second() {
	while(!isDispatchListEmpty) {
		sleep(1);
		currentTime++;
	}
	return NULL;
}

/* Queues processes that have arrived in dispatch list */
void queueProcesses() {
	int i;
	for(i=0; i < processCount; i++ ) {
		if(process[i].arrivalTime <= currentTime) {
			if( (process[i].priority == 0) && !(process[i].queued)) {
				enqueue(&dispatcher.real_time_queue, i);
				process[i].queued = 1;
			}
			else if( (process[i].priority == 1) && !(process[i].queued)) {
				enqueue(&dispatcher.pr1_queue, i);
				process[i].queued = 1;
			}
			else if( (process[i].priority == 2) && !(process[i].queued)) {
				enqueue(&dispatcher.pr2_queue, i);
				process[i].queued = 1;
			}
			else if( (process[i].priority == 3) && !(process[i].queued)) {
				enqueue(&dispatcher.pr3_queue, i);
				process[i].queued = 1;
			}
		}
	}
	
	//isDispatchListEmpty = 1;
	return;
}
	
		
		
