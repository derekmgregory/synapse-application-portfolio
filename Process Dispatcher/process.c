#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[]) {
	
	int remainingTime;
	int priority;
	int id;
	int status;
	
	if(argc != 5) {
		fprintf(stderr, "ERROR: Invalid number of arguments\n");
		exit(1);
	}
	
	/* Get process CPU time */
	
	id = atoi(argv[1]);
	priority = atoi(argv[2]);
	remainingTime = atoi(argv[3]);
	status = atoi(argv[4]);
	
		
	if(priority == 0) {
		printf("Process %i has started executing...\n", id);
		while(remainingTime != 0) {
			printf("Remaining CPU time: %i sec\n", remainingTime);
			sleep(1);
			remainingTime--;
		}	
		printf("Process %i terminated\n\n", id);
		kill(getpid(), SIGINT);
	}
	else {
		while(1){
			printf("Process %i has started executing...\n", id);
			
			printf("Remaining CPU time: %i sec\n", remainingTime);
			sleep(1);
			remainingTime--;
			if(remainingTime == 0) {
				printf("Process %i terminated\n\n", id);
				kill(getpid(), SIGINT);
			}
			else {
				printf("Process %i has suspended\n\n", id);
				kill(getpid(), SIGTSTP);
				printf("Process %i has resumed execution...\n", id);
			}
		}	
	}

	return -1;	
}
