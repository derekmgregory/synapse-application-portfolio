#ifndef MMU_H
#define MMU_H

#include "hostd.h"

typedef struct mmu{
    int remainingMemory;
    char memory[1024];
}mmu;

void memoryAllocator(int procID, mmu * mainMemory, Process p[NUMBER_OF_PROCESSES]);
void memoryInitializer(mmu * mainMemory );
void memoryReleaser(int procID, mmu * mainMemory,  Process p[NUMBER_OF_PROCESSES]);

#endif
