#ifndef HOSTD_H
#define HOSTD_H

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "queue.h"
#include <unistd.h>

typedef struct Dispatcher {
	Queue real_time_queue;
	Queue pr1_queue;
	Queue pr2_queue;
	Queue pr3_queue;
} Dispatcher;

typedef struct Resources {
	int printerNum;
	int scannerNum;
	int modemNum;
	int CDNum;
} Resources;

typedef struct Process {
	int arrivalTime; 
	int priority;
	int procTime;
	int Mbytes;
	int queued;
	int memLocation;
	int alive;
	pid_t process_id;
	Resources rsrc;
} Process;

/* Function Prototypes */
void readFile(FILE *, int);
void printJobParams(int i);
void queueProcesses();
void * counter_1_second();
bool resourceAvailable(int p);
void runProcess(int p, Queue * queue);
	

#endif
