#include "queue.h"
#include "hostd.h"

extern Process process[NUMBER_OF_PROCESSES];

/* Enqueues x into queue.
   Assumes x >= 0.
   Returns 1 if successful and 0 otherwise. */
int enqueue(Queue * q, int x) {
	if(q->firstEmptyIndex >= NUMBER_OF_PROCESSES) return 0;
	q->data[q->firstEmptyIndex] = x;
	q->firstEmptyIndex++;
	return 1;
}

/* Dequeues queue and returns first element.
   Assumes queue only stores values >= 0.
   If no element in queue, then returns -1. */
int dequeue(Queue * q) {
	int i;
	if(q->data[0] == EMPTY_ELEMENT) return -1;
	int dequeued = q->data[0];
	i = 0;
	while(i < (NUMBER_OF_PROCESSES - 1) && q->data[i] != EMPTY_ELEMENT) {
		q->data[i] = q->data[i+1];
		i++;
	}
	q->firstEmptyIndex--;
	return dequeued;
}

/*
*	Checks whether the process is running or
*	needs to be run, and changes its priority
*	appropriately.
*	Dequeue's if process is moved or terminated
*/
void updateQueue( Queue * q1, Queue * q2,  int resource_check){
	if(  (process[q1->data[0]].alive == 1) || !resource_check ){
		//Update priority
		if(process[q1->data[0]].priority < 3)
			process[q1->data[0]].priority++;

		enqueue(q2, q1->data[0]);
	}
	dequeue(q1);
}
