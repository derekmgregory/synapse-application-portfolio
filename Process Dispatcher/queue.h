#ifndef QUEUE_H
#define QUEUE_H

#define NUMBER_OF_PROCESSES 1000
#define EMPTY_ELEMENT -1

typedef struct Queue {
	int data[NUMBER_OF_PROCESSES];
	int firstEmptyIndex;
} Queue;

int enqueue(Queue * queue, int x);
int dequeue(Queue * queue);
void updateQueue( Queue * q1, Queue * q2, int);

#endif
