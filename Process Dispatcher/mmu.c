#include "mmu.h"
#include "hostd.h"


void memoryInitializer(mmu * mainMemory ){
    int i;
    for(i=0; i < 1024; i++)
        mainMemory->memory[i] =0;
    mainMemory->remainingMemory = 960;
    return;
}

void memoryAllocator(int procID, mmu * mainMemory, Process p[NUMBER_OF_PROCESSES]){
    int i; //iterator
    int freeSpace = 0; //keep track of contiguous memory
    int memStart; //keep track of first address of contiguous memory
    int free = 1; //resets memstart
    int difference; //difference between size of new process and the one it is replacing
   
    if(p[procID].priority == 0){ //replacing real time processes
        p[procID].memLocation = 0;
        
        for(i = 0 ; i < p[procID].Mbytes;i++){
            mainMemory->memory[i] = 1; 
        }


        for(i = 64 - p[procID].Mbytes; i < 64; i++){
            mainMemory->memory[i] = 0; // free unused memory
        }
        return;
    }
    else if( p[procID].Mbytes < mainMemory->remainingMemory){ //if there might be enough free memory, try to allocate based on first fit
        i = 64; //first 64 mb reserved for real time
        while( i < 1024 ) { //checking all memory
            if(mainMemory->memory[i] == 0 ){ //if memory adress is free

                if( free == 1 ){
                    memStart = i; //on first free memory address set memstart
                    free =0;
                }

                freeSpace++; //increment free space and i
                i++;

                 if(freeSpace == p[procID].Mbytes){ //if there is enough contiguous free space break to allocate
                     break;
                }
            }
            else{
                free = 1; //reset free so memstart will be correct
                i++;
            }
        }

        if(free == 0){ //allocate memory only if there is enough contiguous space
            
            p[procID].memLocation = memStart;

            for(i = memStart; i < (p[procID].Mbytes + memStart) ; i++){
                 mainMemory->memory[i] = 1; // indicate memory is used
            }

            mainMemory->remainingMemory -= p[procID].Mbytes; //subtract used memory from remaining

            return ;
        }

    }
    else{
     //if there is not enough free memory or contigous space find a process to replace
        for(i = 0; i < 1000; i++){ //check all processes
            if( (p[i].memLocation != -1) && (p[i].Mbytes >= p[procID].Mbytes) ) //if a process has a less important priority  and has sufficient space it will be replaced
                difference = p[i].Mbytes - p[procID].Mbytes; //find difference in processes

                p[procID].memLocation = p[i].memLocation;

                p[i].memLocation = -1;

                for(i = (p[procID].memLocation + p[procID].Mbytes) ; i < (p[procID].memLocation + p[i].Mbytes); i++){
                    mainMemory->memory[i] = 0; //free up unused space
                }
                mainMemory->remainingMemory += difference; // add unused memory to remaining memory
                break;
        } 
       return;
    
    }
    return;
}

void memoryReleaser( int procID, mmu * mainMemory, Process p[NUMBER_OF_PROCESSES] ){
    int i; 
    for( i= p[procID].memLocation ; i < p[procID].Mbytes; i++){ //Free up memory allocated to process
        mainMemory->memory[i] = 0;
    }
    p[procID].memLocation = -1;

    mainMemory->remainingMemory += p[procID].Mbytes; //add unused space to remaining memory
    return;
}
