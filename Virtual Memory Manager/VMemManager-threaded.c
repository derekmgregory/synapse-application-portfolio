#include "Functions.h"
#include <stdio.h>
#include <stdlib.h>
//#include <linux/types.h>
#include <string.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>

#define VERBOSE 1
#define FILEPATH "BACKING_STORE.bin"
#define FILESIZE 65536
#define PAGES 256
#define FRAMES 256
#define FRAMESIZE 256
#define TLB_SIZE 16
#define NUM_ADDR 1000


/* Defines a frame in physical memory */
typedef struct frame {
        char bytes[FRAMESIZE];
} Frame;

/* Our memory and all assorted data structures, eg. page table, TLB etc. */
struct memoryinfo{
    /* Following allows for only one thread to update our page table/memory/TLB etc. */
    pthread_mutex_t lock;   /* Mutex used to lock/unlock isFree */
    pthread_cond_t memoryFree; /* condition variable used for signal unlocking. */
    int isFree;     /* used to check if memory info is being written to/accessed */

    short availableFrame; /* Stores first available page frame in physical memory */
    int firstEmptyEntry; /* Stores index of first empty entry in TLB, -1 otherwise */

    char * backStore;   /* points to a mmap of BACKING_STORE.bin, our simulated disk */

    /* Data representations */
    short pageTable[PAGES]; /* Page Table */
    int TLB[TLB_SIZE][2]; /* TLB - Col 1: Page / Col 2: Frame */
    Frame physicalMem[FRAMES]; /* Physical Memory */


    int TLB_least_recent[TLB_SIZE]; /* Holds recent use time for TLB for LRU algorithm */
    int current_time;   /* Time incremented with every memory access */

    int pageFaultCount; /* Stores the number of page faults that occurred */
    int TLBHitCount; /* Stores the number of TLB Hits that occurred */

} MemoryInfo;


void * memory_access( void * arg ){

    short frameNumber; /* Stores frame number corresponding to a page number */
    int logAddr = (int) arg; /* Logical address */
    int pageNum; /* Stores logical address page number */
    int offset; /* Stores logical address offset */
    int TLB_HIT = 0; /* 1 indicates TLB hit and 0 indicates TLB miss */
    int j;
    int TLB_replacement;

    pageNum = ExtractPageNumber(logAddr); /* Extracts page number from logical address */
    offset = ExtractOffset(logAddr); /* Extracts offset from logical address */
    
    if(VERBOSE)
            printf("Page Number: %i, Offset: %i\n", pageNum, offset);
    
    pthread_mutex_lock(&MemoryInfo.lock);
    while( !MemoryInfo.isFree ){
        pthread_cond_wait(&MemoryInfo.memoryFree, &MemoryInfo.lock);
    }
    MemoryInfo.isFree = 0;
    pthread_mutex_unlock(&MemoryInfo.lock);

    /* Increment current_time because we are accessing memory, for LRU timestamp */
    MemoryInfo.current_time += 1;

    /* Check to see if page number is in TLB */
    for(j = 0; j < TLB_SIZE; j++) {
            if(MemoryInfo.TLB[j][0] == pageNum) {
                    if(VERBOSE) printf("TLB HIT\n");
                    MemoryInfo.TLBHitCount++;
                    TLB_HIT = 1;
                    frameNumber = MemoryInfo.TLB[j][1];
                    MemoryInfo.TLB_least_recent[j] = MemoryInfo.current_time;
                    break;
            }
    }
                    
    /* If a TLB miss occurs check the page table */
    if(!TLB_HIT) {
            if(VERBOSE) printf("TLB MISS\n");
           
            if(MemoryInfo.pageTable[pageNum] == -1) {
            /* Empty page table entry */
                        if(VERBOSE) printf("PAGE FAULT\n");
                        
                        MemoryInfo.pageFaultCount++;
                        MemoryInfo.availableFrame++;
                        
                        /* Read a 256-byte page from the file BACKING_STORE and store
                           it in the first available page frame in physical memory */
                        for(j = 0; j < FRAMESIZE; j++) {
                                MemoryInfo.physicalMem[MemoryInfo.availableFrame].bytes[j] 
                                    = MemoryInfo.backStore[pageNum*FRAMESIZE + j];
                        }
                        MemoryInfo.pageTable[pageNum] = MemoryInfo.availableFrame; /* Update page table */
                        
                }
                
                frameNumber = MemoryInfo.pageTable[pageNum];
                
                /* If there is an empty TLB entry then update the first available entry */
                if((MemoryInfo.firstEmptyEntry = TLB_Empty_Entry_Check(TLB_SIZE, 2, MemoryInfo.TLB)) >= 0) {
                        /* Update first empty TLB entry */
                        MemoryInfo.TLB[MemoryInfo.firstEmptyEntry][0] = pageNum;
                        MemoryInfo.TLB[MemoryInfo.firstEmptyEntry][1] = frameNumber; 
                }
                /* Else if no available TLB entries, select the least recently used*/
                else {
                        TLB_replacement = TLB_least_recently_used( TLB_SIZE, MemoryInfo.TLB_least_recent);
                        MemoryInfo.TLB[TLB_replacement][0] = pageNum;
                        MemoryInfo.TLB[TLB_replacement][1] = frameNumber;
                        MemoryInfo.TLB_least_recent[TLB_replacement] = MemoryInfo.current_time;
                }
        }

    /* Free access to our protected memory variables (page table, TLB, etc.) */
    pthread_mutex_lock(&MemoryInfo.lock);
    MemoryInfo.isFree = 1;
    pthread_cond_signal(&MemoryInfo.memoryFree);
    pthread_mutex_unlock(&MemoryInfo.lock);

    printf("Virtual Address: %5i \t Physical Address: %5i \t Value: %5i\n\n",
                    logAddr, frameNumber*FRAMESIZE + offset, 
                    MemoryInfo.physicalMem[frameNumber].bytes[offset]);

    return NULL;
}


int main(int argc, char **argv) {
    
    /* File descriptors */
    FILE * file_addr;
    int file_backStore;

    int i;
    int logAddr;
    pthread_t thread[NUM_ADDR];

    /* Initializes the mutex and condition variable for the shared memory info */
    if( pthread_mutex_init(&MemoryInfo.lock, NULL) != 0) 
            fprintf(stderr, "ERROR: Could not initialize mutex\n");
    if( pthread_cond_init(&MemoryInfo.memoryFree, NULL) != 0)
            fprintf(stderr, "ERROR: Could not initialize condition variable\n");
    MemoryInfo.isFree = 1;
               
    if(argc != 2) {
            fprintf(stderr, "ERROR: No input file specified\n");
            exit(-1);
    }
    
    if(!(file_addr = fopen(argv[1], "r"))) {
            fprintf(stderr, "ERROR: Unable to open file \"%s\"\n", argv[1]);
            exit(-1);
    }
    
    if((file_backStore = open(FILEPATH, O_RDONLY)) < 0) {
            fprintf(stderr, "ERROR: Unable to open file \"%s\"\n", FILEPATH);
            exit(-1);
    }
      
    /* mmapping the BACK_STORE.bin file */
    MemoryInfo.backStore = mmap(0, FILESIZE, PROT_READ, MAP_SHARED, file_backStore, 0);
    if(MemoryInfo.backStore == MAP_FAILED) {
            fprintf(stderr, "ERROR: Could not mmap %s\n", FILEPATH);
            fclose(file_addr);
            close(file_backStore); 
            exit(-1);
    }

    /* Initialize all elements of pageTable and TLB to -1 to indicate empty table entries */
    memset(MemoryInfo.pageTable, -1, sizeof(MemoryInfo.pageTable)); 
    memset(MemoryInfo.TLB, -1, sizeof(MemoryInfo.TLB)); 

    /* Initialize LRU timestamps to 0 */
    memset(MemoryInfo.TLB_least_recent, 0, sizeof(MemoryInfo.TLB_least_recent));
    MemoryInfo.current_time = 0;

    MemoryInfo.availableFrame = -1;
    fseek(file_addr, 0, 0);
    MemoryInfo.pageFaultCount = 0;
    MemoryInfo.TLBHitCount = 0;

    /* Get addresses from file and create a thread to translate to physical address
        and access the memory at the specified location */
    for(i = 0; i < NUM_ADDR; i++) {
            fscanf(file_addr, "%i", &logAddr); /* Reads logical address from file */
            if( pthread_create(&thread[i], NULL, memory_access, (void*)logAddr) != 0) /*Creates thread with logAddr given as argument */
                fprintf(stderr, "ERROR: Could not create thread %d\n", i);
            pthread_join(thread[i], 0);

    }
    
    printf("Number of addresses translated: %i\n", NUM_ADDR);
    printf("Page Fault Count: %i\n", MemoryInfo.pageFaultCount);
    printf("Page Fault Rate: %.3f\n", (double)MemoryInfo.pageFaultCount / NUM_ADDR);
    printf("TLB Hit Count: %i\n", MemoryInfo.TLBHitCount);
    printf("TLB Hit Rate: %.3f\n", (double)MemoryInfo.TLBHitCount / NUM_ADDR);
            
    if (munmap(MemoryInfo.backStore, FILESIZE) == -1) {
            fprintf(stderr, "ERROR: Could not un-mmap the file");
    }
            
        fclose(file_addr);
        close(file_backStore);    
    
    return 0;
}
