#ifndef FUNCTIONS_H
#define FUNCTIONS_H

int ExtractPageNumber(int addr);
int ExtractOffset(int addr);
int TLB_Empty_Entry_Check(int rows, int cols, int TLB[rows][cols]);
int TLB_least_recently_used( int size_TLB, int * TLB_LRU );

#endif
