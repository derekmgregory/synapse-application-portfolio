#include "Functions.h"

/* Returns page number from logical address */
int ExtractPageNumber(int addr) {
	int pageNumber;
	pageNumber = (addr >> 8) & 0xFF;
	return pageNumber;
}

/* Returns offset from logical address */
int ExtractOffset(int addr) {
	int offset;
	offset = addr & 0xFF;
	return offset;
}

/* Returns index of first empty entry in TLB, otherwise -1 if no empty entries */
int TLB_Empty_Entry_Check(int rows, int cols, int TLB[rows][cols]) {
	int i;
	for(i = 0; i < rows; i++) {
		if(TLB[i][0] == -1) return i;
	}
	return -1;
}

/* Returns the index of the least recently used page in the TLB, or 0 if all the same */
int TLB_least_recently_used( int size_TLB, int * TLB_LRU ) {
	int i;
	int return_index = 0;
	int time_of_use = 0;

	for(i = 0; i < size_TLB; i++) {
		if( TLB_LRU[i] < time_of_use )
			return_index = i;
	}
	return return_index;
}
