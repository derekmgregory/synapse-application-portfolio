/*
For version 1: execute program normally ("./ksamp")
For version 2: execute program with "-s" as a parameter ("./ksamp -s")
For version 3: execute program with "-l" (the letter l) as a parameter as well as the 
		loadavg period (1) and total loadavg time interval (2) ("./ksamp -l <1> <2>")

*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <time.h>

#define MAX_DISPLAY_LEN 100


int versionCheck(int, char**);
void showVersion1();
void showVersion2();
void showVersion3(int, int);


int main(int argc, char *argv[])
{
	
	int version = 1;
	
	
	/*check which version user wants*/
	version = versionCheck(argc, argv);


	/*Perform stat outputs depending on what verson user chose*/
	//user chose version 1
	if(version == 1){
		showVersion1();
	}
	//user chose version 2
	else if(version == 2){
		showVersion1();
		showVersion2();
	
	}
	//user chose version 3
	else if(version == 3){
		showVersion1();
		showVersion2();
		showVersion3(atoi(argv[2]),atoi(argv[3]));
		
	}
	//invalid command inputted by the user
	else{
		printf("invalid command \n");
	}




	return 0;

}

// Function that displays the OS stats required for Version 1 of the program
void showVersion1(){
	FILE * fid;
	int i;
	char uptime[MAX_DISPLAY_LEN];
	char versionStr1[MAX_DISPLAY_LEN];
	char versionStr2[MAX_DISPLAY_LEN];
	char versionStr3[MAX_DISPLAY_LEN];
	char proctypeStr[MAX_DISPLAY_LEN];


	/*determine processor type by accessing 'cpuinfo' file and scanning the
	  5th line ("model name"), which contains the necessary info*/
	fid = fopen("/proc/cpuinfo", "r");
	for(i = 0; i < 4; i++){
		fscanf(fid, "%*[^\n]\n", NULL);
	}
	fscanf(fid, "%*s %*s %*s", NULL);
	fgets(proctypeStr, MAX_DISPLAY_LEN, fid);
	fclose (fid);

	/*access 'uptime' file and scan the first field, as that contains the time duration since
	  boot */
	fid = fopen("/proc/uptime", "r");
	fscanf(fid, "%s", uptime);
	fclose (fid);

	/*access 'version' file and scan the first 3 fields as they contain the info of
	  the kernel version */
	fid = fopen("/proc/version", "r");
	fscanf(fid, "%s %s %s", versionStr1, versionStr2, versionStr3);
	fclose (fid);

	printf("Processor Type: %s", proctypeStr);
	printf("Kernel Version: %s %s %s \n", versionStr1, versionStr2, versionStr3);
	printf("System was last booted %s seconds ago \n", uptime);


}


// Function that displays OS stats required for version 2
void showVersion2(){


	FILE * fid;
	int i;
	int totalRead = 0;
	int totalWrite = 0;
	char userModeTime[MAX_DISPLAY_LEN];
	char systemModeTime[MAX_DISPLAY_LEN];
	char idleTime[MAX_DISPLAY_LEN];
	char check[MAX_DISPLAY_LEN] = " ";
	char numProcesses[MAX_DISPLAY_LEN];
	char numCtxtSwitches[MAX_DISPLAY_LEN];
	char startup[MAX_DISPLAY_LEN];
	char readReqs[MAX_DISPLAY_LEN];
	char writeReqs[MAX_DISPLAY_LEN];

	time_t now; //time_t struct to be used for determining current time
	/*tm struct stores various time info such as seconds, minutes and hours*/
	struct tm * upClock = malloc(MAX_DISPLAY_LEN); //the time when system was booted
	struct tm * realClock;	//the actual time
	int seconds;
	int minutes;
	int hours;
	int result;
	int userhz;	

	time(&now);
	realClock = localtime(&now);
	
	/*access 'uptime' file to determine time duration since last boot, in seconds*/
	fid = fopen("/proc/uptime", "r");
	fscanf(fid, "%s", startup);

	/*use the time duration since last boot to calculate how long the system has
	  been up, in hours:minutes:seconds */
	upClock->tm_sec = (atoi(startup)%60);
	upClock->tm_min = (atoi(startup)/60)%60;
	upClock->tm_hour = (atoi(startup)/60)/60;	
	upClock->tm_year = realClock->tm_year;
	upClock->tm_mon = realClock->tm_mon;
	upClock->tm_yday = realClock->tm_yday;
	upClock->tm_isdst = realClock->tm_isdst;

	/*result will hold the difference between current time and time system has been up.
	  This difference is the time when the system was booted*/
	result = difftime(mktime(realClock),mktime(upClock));
		
	/*convert result to hours:minutes:seconds format*/
	seconds =  result%60;
	minutes = (result/60)%60;
	hours = ((result/60)/60)%24;
	free(upClock);
	fclose(fid);
	
	/*access 'stat' file, and scan fields 1,3,4 as they contain info on the time spent in
	  user mode, system mode, idle, respectively */
	fid = fopen("/proc/stat", "r");
	fscanf(fid, "%*s %s %*s %s %s ", userModeTime, systemModeTime, idleTime);
	fclose(fid);


	/*access 'stat' file and locate 'ctxt' line as that contains the number of context switches
	  made */
	fid = fopen("/proc/stat", "r");
	while(strcmp(check, "ctxt") != 0){
		fscanf(fid, "%s", check);
	}
	fscanf(fid, "%s", numCtxtSwitches);
	strcpy(check, "");
	fclose(fid);

	/*access 'stat' file and locate 'processes' line as that contains the number of processes that
	  have been created */
	fid = fopen("/proc/stat", "r");
	while(strcmp(check, "processes") != 0){
		fscanf(fid, "%s", check);
	}
	fscanf(fid, "%s", numProcesses);
	strcpy(check, "");
	fclose(fid);

	/*access 'diskstats' file and scan fields 4 and 8 as they contain the number of read requests and write requests, respectively. Then sum all of those requests numbers for each device*/
	fid = fopen("/proc/diskstats", "r");
	while(fscanf(fid,"%*s %*s %*s %s %*s %*s %*s %s %*[^\n]\n", readReqs, writeReqs)!= EOF){
		totalRead += atoi(readReqs);
		totalWrite += atoi(writeReqs);
	}
	fclose(fid); 


	/*Check what the USER_HZ is for the user's computer*/
	userhz = sysconf(_SC_CLK_TCK);

	printf("Time Spent in User Mode: %s jiffies (~1/%ith of a second) \n", userModeTime,userhz);
	printf("Time Spent in System Mode: %s jiffies (~1/%ith of a second) \n", systemModeTime,userhz);
	printf("Time Spent Idle: %s jiffies (~1/%ith of a second) \n", idleTime,userhz);
	printf("Number of disk read requests made: %i \n", totalRead);
	printf("Number of disk write requests made: %i \n", totalWrite);
	printf("Number of Context Switches Made: %s \n", numCtxtSwitches);
	printf("System last booted at %i:%i:%i \n", hours, minutes, seconds);
	printf("Number of Processes: %s \n", numProcesses);




}



// Function that displays OS stats that are required in version 3
void showVersion3(int sampleInterval, int observeTime){

	FILE * fid;
	int i;

	char memTotal[MAX_DISPLAY_LEN];
	char memFree[MAX_DISPLAY_LEN];
	char loadAvg[MAX_DISPLAY_LEN];
	char check[MAX_DISPLAY_LEN] = "";
	int numOfSamples  = observeTime/sampleInterval;
	int leftoverTime = observeTime % sampleInterval;
	int count = 0;


	/*access 'meminfo' file and look for Memtotal line to get the total memory available*/
	fid = fopen("/proc/meminfo", "r");
	while(strcmp(check, "MemTotal:") != 0){
	fscanf(fid, "%s", check);
	}
	fscanf(fid, "%s", memTotal);
	strcpy(check, "");
	fclose(fid);

	/*access meminfo file and look for Memfree line to get free memory available*/
	fid = fopen("/proc/meminfo", "r");
	while(strcmp(check, "MemFree:") != 0){
	fscanf(fid, "%s", check);
	}
	fscanf(fid, "%s", memFree);
	strcpy(check, "");
	fclose(fid);




	
	printf("Total Memory: %s kb\n", memTotal);
	printf("Free Memory: %s kb\n", memFree);
	printf("Load Averages (averaged over the last minute) within a %i s interval, being read every %i s: \n", observeTime, sampleInterval);
	
	/*If user inputs an total observation interval smaller than the sample frequency, then report an error. Otherwise access loadavg, and scan and display the first fields, which shows the load average over the last minute*/
	if(observeTime < sampleInterval)
	{
		printf("Error: Please make sure to input an observation time (second number) greater than sample time interval (first number) \n");
	}
	else{
		
		
		while(count < numOfSamples){
			fid = fopen("/proc/loadavg", "r");
			fscanf(fid, "%s", loadAvg);
			sleep(sampleInterval);
			printf("%s \n", loadAvg);
			fclose(fid);
			count++;
	
		}
			sleep(leftoverTime);
	}


}


/*Assigns a value corresponding to the user's chosen version*/
int versionCheck(int argc, char**argv)
{
	char* arg2 = (char*) malloc(96);
	char* arg3 = (char*) malloc(96);
	int i,j;
	
	
		
		/*Check if the user executed the program normally, with no parameters
		  if they did, then return "1" for version 1 */
		if(argc == 1){
			free(arg2);
			free(arg3);
			return 1;
		}
		/*Check if the user executed the program with 1 parameter passed. If the paramater is
		  "-s", then return "2" to run version 2 of the program. Otherwise, return "0" for
		   an invalid command*/
		else if(argc == 2){
			if(strcmp(argv[1], "-s") == 0){
				free(arg2);
				free(arg3);
				return 2;
			}
			else{
				free(arg2);
				free(arg3);
				return 0;
			}

		}


		/*Check if the user executed the program with 3 parameters passed. If the first paramater is
		  "-l", then check if the next 2 parameters are integer numbers. If both checks suceeed
		  return a "3" to run version 3 of program, otherwise return 0 for an invalid command*/

		else if(argc == 4){
			strcpy(arg2,argv[2]);
			strcpy(arg3,argv[3]);
			
			if(strcmp(argv[1], "-l") == 0){
			
				for(i = 0; i < strlen(arg2); i++)
					if(!isdigit(arg2[i])){
						free(arg2);
						free(arg3);
						return 0;
					}
				for(j = 0; j < strlen(arg3); j++)
					if(!isdigit(arg3[j])){
						free(arg2);
						free(arg3);
						return 0;
					}
				free(arg2);
				free(arg3);
				return 3;	
			}
			free(arg2);
			free(arg3);
			return 0;
		}
		// return a 0 for an invalid command if the user does not enter an approproate number of
		// parameters.
		else{
			free(arg2);
			free(arg3);
			return 0;
		}



	
}




